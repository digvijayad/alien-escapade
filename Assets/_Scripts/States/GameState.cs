﻿/* <summary>
 * Our Game state enum
 * </summary>
 */

public enum GameState
{
	Start,
	Playing,
	Pause,
	Resume,
	Dead
}

