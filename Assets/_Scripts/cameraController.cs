﻿using UnityEngine;
using System.Collections;

public class cameraController : MonoBehaviour {


	public Transform target;
	public Vector3 offset = new Vector3 (0f, 3f, -4f);
	private Vector3 newPosition;

	// Update is called once per frame
	private void LateUpdate () {

		newPosition = new Vector3 (target.position.x, 0, target.position.z);
		transform.position = newPosition + offset;
	}
}
