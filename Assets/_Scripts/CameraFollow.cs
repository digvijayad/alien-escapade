/// <summary>
/// Camera follow.
/// this script use for control main camera to follow target(character)
/// </summary>

using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {
	
	//[HideInInspector]
	private Transform target; //Target to follow
	public float angle = 15; //Angle camera
	public float distance = -4; //Distance target
	public float height = 2.5f; // Height camera
	
	
	//Private variable field
	private Vector3 posCamera;
	private Vector3 initPosCamera;
	private Vector3 initRotCamera;
	private Vector3 angleCam;
	private bool shake;

	private static CameraFollow instance;
	public static CameraFollow Instance{get{ return instance;}}



	void Start(){
		instance = this;

		target = GameManager.Instance.players[(int)GameManager.Instance.currentPlayer].transform;
	
		initPosCamera = transform.position;
		initRotCamera = transform.eulerAngles;

	}
	
	void LateUpdate(){
		if(target != null){
			target = GameManager.Instance.players[(int)GameManager.Instance.currentPlayer].transform;
			if(target.position.z >= -10){
				if(shake == false){
					posCamera.x = Mathf.Lerp(posCamera.x, target.position.x, 10 * Time.deltaTime);
					posCamera.y = Mathf.Lerp(posCamera.y, target.position.y + height, 5 * Time.deltaTime);
					posCamera.z = Mathf.Lerp(posCamera.z, target.position.z + distance, controllerMovement.Instance.speed); //* Time.deltaTime);
					transform.position = posCamera;
					//transform.LookAt (target);
					//angleCam.x = Mathf.Lerp(transform.eulerAngles.x, target.eulerAngles.x, 1*Time.deltaTime);
					angleCam.x = 0;
					angleCam.y = Mathf.Lerp(angleCam.y, 10, 1 * Time.deltaTime);
					angleCam.z = transform.eulerAngles.z;
					transform.eulerAngles = angleCam;
					//transform.eulerAngles = Vector3.Lerp(transform.eulerAngles, angleCam, 1 * Time.deltaTime);
//					if (target.position.z <= 0) {
//						transform.rotation = Quaternion.Slerp (transform.rotation, target.transform.rotation, 5 * Time.deltaTime);
//					} else {
//						angleCam.x = angle;
//						angleCam.y = Mathf.Lerp(angleCam.y, 10, 1 * Time.deltaTime);
//						angleCam.z = transform.eulerAngles.z;
//						transform.eulerAngles = /*Vector3.Lerp*/transform.eulerAngles, angleCam, 1 * Time.deltaTime;
//					}
				}
			}
			else{

				if(GameManager.Instance.gameState.Equals(GameState.Playing))
				{
					posCamera = initPosCamera;
					//transform.LookAt (GameObject.FindGameObjectWithTag("Player").transform);
					transform.LookAt (target);
				}
			}
		}
	}
	
	
	//Reset camera when charater die
	public void Reset(){
		shake = false;
		posCamera = initPosCamera;
		transform.position = posCamera;
		transform.eulerAngles = initRotCamera;
	}
	
	//Shake camera
	public void ActiveShake(){
		shake = true;
		StartCoroutine(ShakeCamera());	
	}
	
	IEnumerator ShakeCamera(){
		float count = 0;
		Vector3 pos = Vector3.zero;;
		while(count <= 0.2f){
			count += 1 * Time.smoothDeltaTime;	
			pos.x = transform.position.x + Random.Range(-0.2f,0.2f);
			pos.y = target.position.y+ height;// + Random.Range(-0.05f,0.05f);
			pos.z = target.position.z + distance;
			transform.position = pos;
			yield return 0;
		}
		transform.position = posCamera;
		posCamera.x = transform.position.x;
		posCamera.y = target.position.y + height;
		posCamera.z = target.position.z + distance;
		transform.position = posCamera;
		angleCam.x = angle;
		angleCam.y = transform.eulerAngles.y;
		angleCam.z = transform.eulerAngles.z;
		transform.eulerAngles = angleCam;
		shake = false;
	}
	
}
