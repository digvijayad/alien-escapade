﻿using UnityEngine;
using System.Collections;

public class renderTextureScript : GenericSingletonClass<renderTextureScript> {

	/// <summary>
	/// The players need to be placed 5 units away from each other in the order they are at the char Selection texture sprite
	/// </summary>
	public float xLookAt = 0f;

	void Update () 
	{
		transform.position = new Vector3 (xLookAt, transform.position.y, transform.position.z);
	}
}
