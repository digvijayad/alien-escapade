﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/// <summary>
/// Character selection menu script. Loads the buttons for character Selection 
/// when the user enters the charselectionmenu
/// </summary>
public class CharSelectionMenu : MonoBehaviour {

	public GameObject charButtonPrefab;
	public GameObject charButtonContainer;

	private int previousSelectedIndex;

	//Assign bit flag values to choose players

	Sprite coloredCheckedImage ;
	Sprite unColoredCheckedImage;
	private void Start(){
		coloredCheckedImage = Resources.Load <Sprite>("UI/checked");
		unColoredCheckedImage = Resources.Load <Sprite>("UI/checked_unselected");

		UIManager.Instance.charSelectionEnergyBallText.text = GameManager.Instance.totalEnergyBalls.ToString (); 

		//store the currentSelected Index for reference
		previousSelectedIndex = (int)GameManager.Instance.currentPlayer;

		//Load all player Sprites
		Sprite[] playerTypeTextures = Resources.LoadAll<Sprite> ("Players");
		for (int i = 0; i < playerTypeTextures.Length; i++) {

			GameObject container = Instantiate (charButtonPrefab) as GameObject;
			container.GetComponent<Image> ().sprite = playerTypeTextures [i];
			container.transform.SetParent (charButtonContainer.transform, false);

			int index = i;
			//check if the skin is already bought, if yes, mark it.
			if ((GameManager.Instance.playerAvailability & 1 << index) == 1 << index){
				container.transform.GetChild (0).gameObject.SetActive (true);

				//change the image of the current selected Player to colored checked image
				if (previousSelectedIndex.Equals (index)) {
					container.transform.GetChild (0).GetComponentInChildren<Image> ().sprite = coloredCheckedImage;
				}
			}
			container.GetComponent<Button> ().onClick.AddListener (() => changePlayer (index));
		}

	}
	void Update(){
		UIManager.Instance.charSelectionEnergyBallText.text = GameManager.Instance.totalEnergyBalls.ToString ();
	}
	private void changePlayer(int index){
		//Check if the player has bought the skin through player availability using bit flags
		renderTextureScript.Instance.xLookAt = index * 5;
		if ((GameManager.Instance.playerAvailability & 1 << index) == 1 << index) {

			//change the current selected player image selected icon
			if (!previousSelectedIndex.Equals (index)) {
				charButtonContainer.transform.GetChild(index).GetChild(0).GetComponentInChildren<Image> ().sprite = coloredCheckedImage;
				charButtonContainer.transform.GetChild(previousSelectedIndex).GetChild(0).GetComponentInChildren<Image> ().sprite = unColoredCheckedImage;	
				previousSelectedIndex = index;
			}
			GameManager.Instance.activatePlayer ((CurrentPlayer)index);

		} else {
			/*You do not have the skin, do you want to buy it? */
			int cost = 10000;
			if (GameManager.Instance.totalEnergyBalls >= cost) {
				GameManager.Instance.totalEnergyBalls -= cost;
				GameManager.Instance.playerAvailability += 1 << index;
				GameManager.Instance.Save ();
				charButtonContainer.transform.GetChild (index).GetChild (0).gameObject.SetActive (true);
			}

		}
	}

}
