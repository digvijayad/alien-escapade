using UnityEngine;
using System.Collections;

public class colliderScript : MonoBehaviour {
	private CharacterController controller;

	// Use this for initialization
	void Start () {
		controller = GetComponent <CharacterController> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnControllerColliderHit(ControllerColliderHit hit){
		if (hit.gameObject.CompareTag ("Obstacles")) {
			GameManager.Instance.Die ();
		}
	}
}
