﻿/// <summary>
/// Controller.
/// this script use for control a character
/// </summary>
using UnityEngine;
using System.Collections;

public class controllerMovement : MonoBehaviour {

	public enum DirectionInput{
		Null, Left, Right, Up, Down
	}
	public enum Position {
		Middle = 0, Left = -3, Right = 3 
	}
			
	public enum charState
	{
		None, Idle, Run, Slide, Jump
	}
	[HideInInspector]
	public Animator animator;
	[HideInInspector]
	public CharacterController controller;

	private bool activeInput;
	private Vector2 currentPos;

	public bool keyInput;
	public bool touchInput;

	private Position positionStand;
	private DirectionInput directInput;
	//private AnimationManager animationManager;

	public float starterSpeed = 12f;
	public float speed = 12F;
	public float jumpSpeed = 10.0F;
	public float gravity = 20.0F;
	private float SpeedIncreaseZ = 0f;
	private float turn = 0f;
	float moveDirY = 0.0001f;

	float lane;

	private float m_CharHeight;
	private Vector3 m_CharCenter;
	private float m_CharSlideHeight;
	private Vector3 m_CharSlideCenter;
	private static controllerMovement instance;

	public static controllerMovement Instance{
		get
		{
			if (instance == null) {
				instance = GameObject.FindObjectOfType<controllerMovement> ();
			}

			return instance;
		}
	}

	void Start () {
		//Set state character
		controller = GameManager.Instance.players[(int)GameManager.Instance.currentPlayer].GetComponent<CharacterController> ();
		animator = GameManager.Instance.players[(int)GameManager.Instance.currentPlayer].GetComponent<Animator> ();
		lane = 0;
		positionStand = Position.Middle;
		m_CharHeight = controller.height;
		m_CharCenter= controller.center;

	}

	public void Reset(){
		if (!controller.Equals (GameManager.Instance.players [(int)GameManager.Instance.currentPlayer].GetComponent<CharacterController> ())) {
			controller = GameManager.Instance.players [(int)GameManager.Instance.currentPlayer].GetComponent<CharacterController> ();
			animator = GameManager.Instance.players[(int)GameManager.Instance.currentPlayer].GetComponent<Animator> ();
		}

		animator.Play("Idle");

		lane = 0;
		this.speed = starterSpeed;
		transform.GetChild((int)GameManager.Instance.currentPlayer).position = new Vector3 (lane, /*transform.GetChild((int)GameManager.Instance.currentPlayer).position.y*/ 0f, -15f);
		positionStand = Position.Middle;
	}

	void Update () {
		if (!controller.Equals (GameManager.Instance.players [(int)GameManager.Instance.currentPlayer].GetComponent<CharacterController> ())) {
			controller = GameManager.Instance.players [(int)GameManager.Instance.currentPlayer].GetComponent<CharacterController> ();
			animator = GameManager.Instance.players[(int)GameManager.Instance.currentPlayer].GetComponent<Animator> ();
		}
		//Debug.Log (transform);
		switch (GameManager.Instance.gameState)
		{

		case GameState.Start:
			
			UIManager.Instance.SetStatus ("Start");
			//GameManager.Instance.gameState = GameState.Playing;

			break;

		case GameState.Pause:

			UIManager.Instance.SetStatus ("Paused");
		

			break;

		case GameState.Resume:
			GameManager.Instance.gameState = GameState.Playing;
			break;

		case GameState.Dead:
			
			animator.Play ("Idle");
			UIManager.Instance.SetStatus ("Dead");
			break;



		case GameState.Playing:
			
			UIManager.Instance.SetStatus (speed.ToString ());

			if(((int)transform.GetChild((int)GameManager.Instance.currentPlayer).position.z) > 0){
				
				GameManager.Instance.currentScore = (int)transform.GetChild((int)GameManager.Instance.currentPlayer).position.z * GameManager.Instance.multiplier;
				UIManager.Instance.SetScore (GameManager.Instance.currentScore.ToString());
			}

		
			if (keyInput)
				KeyInput ();
			if (touchInput)
				DirectionAngleInput ();

			//Increase the speed every 100 units
			if (transform.GetChild((int)GameManager.Instance.currentPlayer).position.z - SpeedIncreaseZ >= 100f && speed <= 18f) {
				SpeedIncreaseZ = transform.GetChild((int)GameManager.Instance.currentPlayer).position.z;
				speed += 0.2f;
			}

			Move ();

			/* Update the animator values*/
			UpdateAnimator ();

			break;


		default:
			break;
		}

	}

	void Move() {

		/* Putting the Move before isGrounded fixes the issue of alternating isGrounded*/
		controller.Move (new Vector3 (0f, moveDirY, speed) * Time.deltaTime);

		Vector3 newPosition = transform.GetChild((int)GameManager.Instance.currentPlayer).position;
		Vector3 iniPos = newPosition;
		m_CharSlideHeight = m_CharHeight;
		m_CharSlideCenter = m_CharCenter;

		if (controller.isGrounded) {
			moveDirY = 0.0001f;

			if (directInput == DirectionInput.Up) {

				moveDirY = jumpSpeed;
				turn = 0;
				animator.SetBool ("onGround", false);

				SoundManager.instance.PlayingSound("jump");
				Invoke ("stopJump", 0.1f);

			}
			if (directInput == DirectionInput.Down) {
				
				startSlide ();
			}

			if (animator.GetBool ("isSlide").Equals(true) ){
				m_CharSlideHeight = 0.5f * m_CharSlideHeight;
				m_CharSlideCenter.y = 0.5f*m_CharCenter.y;
			} 

			/*Lerp control height*/
			controller.height = Mathf.Lerp (m_CharHeight, m_CharSlideHeight,1f);
			controller.center = m_CharSlideCenter;

			if (directInput == DirectionInput.Left) {
				if (positionStand != Position.Left) {

					if (positionStand == Position.Middle)
						positionStand = Position.Left;
					else if (positionStand == Position.Right)
						positionStand = Position.Middle;

					lane -= 3f;
					transform.GetChild((int)GameManager.Instance.currentPlayer).Rotate (0, -30f, 0);
					animator.StopPlayback ();
					animator.SetBool ("isTurn", true);

					SoundManager.instance.PlayingSound ("jump");
					Invoke ("stopTurn", 0.1f);
				} else {
					CameraFollow.Instance.ActiveShake ();
				}
			}
			if (directInput == DirectionInput.Right) {
				if (positionStand != Position.Right) {

					if (positionStand == Position.Middle)
						positionStand = Position.Right;
					else if (positionStand == Position.Left)
						positionStand = Position.Middle;
					lane += 3f;
					transform.GetChild((int)GameManager.Instance.currentPlayer).Rotate (0, 40f, 0);

					animator.SetBool ("isTurn", true);
					SoundManager.instance.PlayingSound ("jump");
					Invoke ("stopTurn", 0.1f);
				} else {
					CameraFollow.Instance.ActiveShake ();
				}
			}

		} else {

			if (directInput.Equals (DirectionInput.Down)) {
				moveDirY -= jumpSpeed * 3;
				startSlide ();
			}

			if (directInput == DirectionInput.Left && positionStand != Position.Left) {
				lane -= 3f;

				if (positionStand == Position.Middle)
					positionStand = Position.Left;
				else if (positionStand == Position.Right)
					positionStand = Position.Middle;

			} else if (directInput == DirectionInput.Right && positionStand != Position.Right) {

				lane += 3f;
				if (positionStand == Position.Middle)
					positionStand = Position.Right;
				else if (positionStand == Position.Left)
					positionStand = Position.Middle;
			}
		}
		newPosition.x = lane;

		/* transform the position for side to side movement*/
		transform.GetChild((int)GameManager.Instance.currentPlayer).position = Vector3.Lerp (iniPos, newPosition, 6 * Time.deltaTime);
		moveDirY -= gravity * Time.deltaTime;
	}


	void startSlide(){
		m_CharSlideHeight = 0.5f * m_CharHeight;
		m_CharSlideCenter.y = 0.5f * m_CharCenter.y;

		if (animator.GetBool ("isSlide").Equals (false)) {
			animator.SetBool ("isSlide", true);
		}
		stopRotation ();
		SoundManager.instance.PlayingSound("jump");
		Invoke ("stopSlide", 0.5f);
	}
		
	void UpdateAnimator(){
		animator.SetFloat ("Jump", moveDirY);
		animator.SetFloat ("JumpLeg", turn);

	}
	public void stopJump(){
		animator.SetBool ("onGround", true);
		Invoke ("stopRotation", 0.1f);
	}

	public void stopTurn(){
		animator.SetBool ("isTurn", false);
		Invoke ("stopRotation", 0f);
	}

	private void stopRotation(){
		transform.GetChild((int)GameManager.Instance.currentPlayer).rotation = new Quaternion (0, 0, 0, 0);	
	}
	public void stopSlide(){
		animator.SetBool ("isSlide", false);
		Invoke ("stopRotation", 0f);
	}
	 
	//Key input method
	private void KeyInput()
	{
		if(Input.anyKeyDown)
		{
			activeInput = true;
		}

		//if(activeInput && checkSideCollision == false)
		if(activeInput)
		{
			if(Input.GetKey(KeyCode.LeftArrow))
			{
				directInput = DirectionInput.Left;
				activeInput = false;
			}else

				if(Input.GetKey(KeyCode.RightArrow))
				{
					directInput = DirectionInput.Right;
					activeInput = false;
				}else

					if(Input.GetKey(KeyCode.UpArrow))
					{
						directInput = DirectionInput.Up;
						activeInput = false;
					}else

						if(Input.GetKey(KeyCode.DownArrow))
						{
							directInput = DirectionInput.Down;
							activeInput = false;
						}
		}else
		{
			directInput = DirectionInput.Null;	
		}

	}
	private void DirectionAngleInput(){
		if(Input.GetMouseButtonDown(0)){
			currentPos = Input.mousePosition;
			activeInput = true;
		}

		if(Input.GetMouseButton(0)){
			if(activeInput){
				float ang = CalculateAngle.GetAngle(currentPos, Input.mousePosition);
				if((Input.mousePosition.x - currentPos.x) > 20){
					if(ang < 45 && ang > -45){
						directInput = DirectionInput.Right;
						activeInput = false;
					}else if(ang >= 45){
						directInput = DirectionInput.Up;
						activeInput = false;
					}else if(ang <= -45){
						directInput = DirectionInput.Down;
						activeInput = false;
					}
				}else if((Input.mousePosition.x - currentPos.x) < -20){
					if(ang < 45 && ang > -45){
						directInput = DirectionInput.Left;
						activeInput = false;
					}else if(ang >= 45){
						directInput = DirectionInput.Down;
						activeInput = false;
					}else if(ang <= -45){
						directInput = DirectionInput.Up;
						activeInput = false;
					}
				}else if((Input.mousePosition.y - currentPos.y) > 20){
					if((Input.mousePosition.x - currentPos.x) > 0){
						if(ang > 45 && ang <= 90){
							directInput = DirectionInput.Up;
							activeInput = false;
						}else if(ang <= 45 && ang >= -45){
							directInput = DirectionInput.Right;
							activeInput = false;
						}
					}else if((Input.mousePosition.x - currentPos.x) < 0){
						if(ang < -45 && ang >= -89){
							directInput = DirectionInput.Up;
							activeInput = false;
						}else if(ang >= -45){
							directInput = DirectionInput.Left;
							activeInput = false;
						}
					}
				}else if((Input.mousePosition.y - currentPos.y) < -20){
					if((Input.mousePosition.x - currentPos.x) > 0){
						if(ang > -89 && ang < -45){
							directInput = DirectionInput.Down;
							activeInput = false;
						}else if(ang >= -45){
							directInput = DirectionInput.Right;
							activeInput = false;
						}
					}else if((Input.mousePosition.x - currentPos.x) < 0){
						if(ang > 45 && ang < 89){
							directInput = DirectionInput.Down;
							activeInput = false;
						}else if(ang <= 45){
							directInput = DirectionInput.Left;
							activeInput = false;
						}
					}

				}
			}
		}

		if(Input.GetMouseButtonUp(0)){
			directInput = DirectionInput.Null;	
			activeInput = false;
		}

	}

}
