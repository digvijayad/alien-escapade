﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
/// <summary>
/// Tile pool.
/// </summary>
[System.Serializable]
public class TilePool{
	public objectPoolers theObjectPool;
	public string tileName;
	public int weight;
}
/// <summary>
/// Tile manager. 
/// The index for SpwanPoints gameObject needs to be 0 in the Hierarchy.
/// </summary>
public class TileManager : MonoBehaviour {

	public TilePool[] tilePools;
	public objectPoolers[] obstaclePools;
	public objectPoolers[] coinPools;

    private Transform playerTransform;
	private int tileSelector;


	/*Spawn location for next tile*/
    private float spawnZ = 0f; 

	/*Length for each tile prefab*/
    private static float tileLength = 100f;  
	public static float TileLength
	{
		get{
			return tileLength;
		}
	}

	/*Total number of tiles on screen*/
    private int amnTilesOnScreen = 4;

    private int lastPrefabIndex = 0;

    private float safeZone = tileLength + 20.0f;

	//Prefab Indexes
	private static int beach1ToCity1TransitionPrefabIndex;
	private static int beach1PrefabIndex;
	private static int city1PrefabIndex;

	private static TileManager instance;
	public static TileManager Instance
	{
		get{
			if (instance == null) {
				instance = GameObject.FindObjectOfType<TileManager> ();
			}

			return instance;
		}
	}

	private bool Missing(){
		
		if (tilePools == null) {
			Debug.Log ("TileManager needs 'TilePool Data'! ");
			enabled = false;
			return true;
		} 
		if (obstaclePools == null) {
			Debug.Log ("TileManager needs Obstacles pools Object");
			enabled = false;
			return true;
		}

		return false;
	}
    // Use this for initialization
    private void Start () {

		if(Missing())
			return;

        //get the indexes of the prefab through names or DO IT MANUALLY
        city1PrefabIndex = System.Array.FindIndex(tilePools,0, tilePools.Length, i => i.tileName == "City1");
        beach1PrefabIndex = System.Array.FindIndex(tilePools,0, tilePools.Length, i => i.tileName == "beach1");
        beach1ToCity1TransitionPrefabIndex = System.Array.FindIndex(tilePools, 0, tilePools.Length, i => i.tileName == "beach1ToCity1");

  //      for (int i=0;i<tilePools.Length;i++) {
			
		//	if( tilePools[i].theObjectPool == null )
		//	{
		//		Debug.Log("TileManager needs theObjectPool object of TilePool!");
		//		enabled = false;
		//		return;
		//	}

		//	//Please make sure the names of prefabs are the same as these
		//	if (tilePools [i].tileName.Equals ("City1")) {
		//		city1PrefabIndex = i;
		//	} else if (tilePools [i].tileName.Equals ("beach1")) {
		//		beach1PrefabIndex = i;
		//	}
		//	else if (tilePools [i].tileName.Equals("beach1ToCity1")){
		//		beach1ToCity1TransitionPrefabIndex = i;
		//	}
		//}
		playerTransform = GameManager.Instance.players[(int)GameManager.Instance.currentPlayer].transform;

    }
		

	// Update is called once per frame
	private void Update () {
		if (!playerTransform.Equals (GameManager.Instance.players [(int)GameManager.Instance.currentPlayer].transform)) {
			playerTransform = GameManager.Instance.players [(int)GameManager.Instance.currentPlayer].transform;
		}

	    if(playerTransform.position.z - safeZone > (spawnZ - amnTilesOnScreen * tileLength))
        {
			tileSelector = getNext();
			GameObject newTile = tilePools[tileSelector].theObjectPool.GetPooledObject ();
			GameObject newObstacle = obstaclePools [(int)Random.Range (0, obstaclePools.Length)].GetPooledObject ();

			newTile.transform.position = Vector3.forward * spawnZ;
			newObstacle.transform.position = Vector3.forward * spawnZ;
			spawnZ += tileLength;

			newTile.SetActive (true);
			newObstacle.SetActive (true);

			int totalSpawnLocations = newObstacle.transform.GetChild (0).childCount;
			int SpawnLocations = Random.Range (2, totalSpawnLocations);

			List<int> spawnPoints = new List<int>(SpawnLocations);

			for (int i = 0; i < SpawnLocations; i++) {
			

				int spawnPoint;
				//Make sure the spawnPoint doesnt' repeat
				do {
					spawnPoint = Random.Range (0, totalSpawnLocations);
				} while(spawnPoints.Contains (spawnPoint));
				spawnPoints.Add (spawnPoint);

				Vector3 location = newObstacle.transform.GetChild (0).GetChild (spawnPoint).position;

				string name = newObstacle.transform.GetChild (0).GetChild (spawnPoint).name;

				GameObject coin;
				if (name.Contains ("jumpPoint")) {
					coin = coinPools [0].GetPooledObject ();
				} else if (name.Contains ("RightPoint")) {
					coin = coinPools [2].GetPooledObject ();
				} else if (name.Contains ("LeftPoint")) {
					coin = coinPools [1].GetPooledObject ();
				}else if(name.Contains("StraightPoint")){
					coin = coinPools [3].GetPooledObject ();
				} else {
					continue;
				}

				coin.transform.position = location;
				coin.SetActive (true);

				for(int j = 0;j < coin.transform.childCount; j++){
					if(!coin.transform.GetChild(j).gameObject.activeSelf){
						coin.transform.GetChild (j).gameObject.SetActive (true);
					}
				}
			}
		
        }
	}

	public void Reset(){

		for (int i = 0; i < this.transform.childCount; i++) {
			if (this.transform.GetChild (i).gameObject.activeSelf.Equals (true)) {
				this.transform.GetChild (i).gameObject.SetActive (false);
			}
		}
		GameObject starterTile = tilePools[0].theObjectPool.GetPooledObject ();
		starterTile.transform.position = Vector3.forward * -100f;

		starterTile.SetActive (true);
		spawnZ = 0f;

	}

	private int getNext(){

		/*Return the city index if lastPrefabIndex was beach to city transition*/
		if (lastPrefabIndex == beach1ToCity1TransitionPrefabIndex) {
			lastPrefabIndex = city1PrefabIndex;
			return city1PrefabIndex;
		}

		/*increase the weight of the previous tile */
		for (int i = 0; i < tilePools.Length; i++) {
			if (tilePools [i].weight != 0) {
				if (i == lastPrefabIndex)
					tilePools [i].weight = 4;
				else {
					tilePools [i].weight = 3;
				}
			}
		}

		int totalWeight = 0;
		for (int i = 0; i < tilePools.Length; i++) {
			totalWeight += tilePools [i].weight;
		}

		int randomIndex = lastPrefabIndex;
		int r = Random.Range (0, totalWeight);
		int index = 0;

		for (int i = 0; i < tilePools.Length; i++) {
			index += tilePools [i].weight;

			if (r < index) {
				randomIndex = i;
				break;
			}
		}

		/*Put the beach to city transition tile*/
		if (lastPrefabIndex == beach1PrefabIndex && randomIndex == city1PrefabIndex) {
			lastPrefabIndex = beach1ToCity1TransitionPrefabIndex;
			return beach1ToCity1TransitionPrefabIndex;
		}

		lastPrefabIndex = randomIndex;

		return randomIndex;

	}

}
