﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SoundManager : MonoBehaviour {

	[System.Serializable]
	public class SoundGroup{
		public AudioClip audioClip;
		public string soundName;
	}

	public AudioSource bgmSound;
	public List<SoundGroup> sound_List = new List<SoundGroup>();
	public static SoundManager instance;

	// Use this for initialization
	void Start () {
		instance = this;

		//Play the background music as soon as started
		bgmSound.Play ();
	}

	public void PlayingSound(string _soundName){
		AudioSource.PlayClipAtPoint(sound_List[FindSound(_soundName)].audioClip, Camera.main.transform.position);
	}

	private int FindSound(string _soundName){
		int i = 0;
		while( i < sound_List.Count ){
			if(sound_List[i].soundName == _soundName){
				return i;	
			}
			i++;
		}
		return i;
	}


	// Update is called once per frame
	void Update () {
	
	}
}
