﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIManager : GenericSingletonClass<UIManager> {

//	public enum MenuStates
//	{
//		MainMenu,
//		gamePlayMenu,
//		pauseMenu,
//		shopMenu,
//		charSelectionMenu,
//		settingsMenu,
//		topRunMenu
//	}
	/*Important for Singleton*/	
	protected UIManager(){
	}

	public void SetStatus(string text){
		statusText.text = text;
	}

	public void SetScore(string text){
		gpScoreText.text = text;
	}

	public void setEnergyBalls(string text){
		gpEnergyBallText.text = text;
	}

	public void setDmScore(string energyBalls, string score )
	{
		dmScoreText.text = score;
		dmEnergyBallText.text = energyBalls;
	}
	public void setTotalEnergyBalls(string text){
		dmTotalEnergyBallText.text = text;
		charSelectionEnergyBallText.text = text;
	}
		
	public void pauseGamePlay(){
		GameManager.Instance.Pause ();
	}

	public void openMainMenu ()
	{
		GameManager.Instance.Reset ();
		Time.timeScale = 1;
		this.Reset ();
	}
		
	public void openShop(){
		Debug.Log ("Shop Clicked");
	}

	public void openCharSelectionMenu(){
		Debug.Log ("char Clicked");
		mainMenu.SetActive (false);
		charSelectionMenu.SetActive (true);

	}
	public void openTopRun(){
		Debug.Log ("Top Run Clicked");
	}

	public void openPlayMenu(){
		
		GameManager.Instance.currentScore = 0;
		GameManager.Instance.currentEnergyBalls = 0;
		SetScore ("0000");
		setEnergyBalls ("0");
		GameManager.Instance.Reset ();
		controllerMovement.Instance.animator.Play ("Run");
		GameManager.Instance.gameState = GameState.Playing;

		mainMenu.SetActive (false);
		deathMenu.SetActive (false);
		gamePlayMenu.SetActive (true);
	}
		
	public void openDeathMenu(){
		
		this.gamePlayMenu.SetActive (false);
		this.deathMenu.SetActive (true);
	}
	public void Reset ()
	{
		this.gamePlayMenu.SetActive (false);
		this.pauseMenu.SetActive (false);
		this.charSelectionMenu.SetActive (false);
		this.deathMenu.SetActive (false);
		this.mainMenu.SetActive (true);

	}
	//Menus here
	public GameObject pauseMenu;
	public GameObject gamePlayMenu;
	public GameObject countDown;
	public GameObject mainMenu;
	public GameObject charSelectionMenu;
	public GameObject deathMenu;

	//Score Implementation here
	public Text statusText;
	public Text countText;
	public Text gpScoreText;
	public Text gpEnergyBallText;
	public Text dmTotalEnergyBallText;
	public Text charSelectionEnergyBallText;
	public Text dmEnergyBallText;
	public Text dmScoreText;
	public Text dmHighScoreText;
}
