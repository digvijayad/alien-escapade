﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Game manager.
/// The game will have the starting screen as the alien inside the ship fiddleing with keys.
/// </summary>
public class GameManager : GenericSingletonClass<GameManager> {
	
	//Game State Enum is accessed from here

	public GameState gameState {get; set;}
	public CurrentPlayer currentPlayer { get; set; }
	public GameObject[] players; 
	//public GameObject player_1;

	public int totalEnergyBalls = 0;
	public int playerAvailability = 1;
	public int highScore = 0;

	//GamePlay variables
	public int multiplier = 1;
	public int currentScore = 0;
	public int currentEnergyBalls = 0;

	private bool pause;

	void Awake() {
		if (PlayerPrefs.HasKey ("CurrentPlayer")) {
			currentPlayer = (CurrentPlayer)PlayerPrefs.GetInt ("CurrentPlayer");
			totalEnergyBalls = PlayerPrefs.GetInt ("EnergyBalls");
			playerAvailability = PlayerPrefs.GetInt ("playerAvailability");
			highScore = PlayerPrefs.GetInt ("HighScore");

		} else {
			currentPlayer = CurrentPlayer.player_0;
			PlayerPrefs.SetInt ("CurrentPlayer",(int)currentPlayer);
			PlayerPrefs.SetInt ("EnergyBalls",totalEnergyBalls);
			PlayerPrefs.SetInt ("playerAvailability", playerAvailability);
			PlayerPrefs.SetInt ("HighScore", highScore);
		}

	}
	void Start(){
		gameState = GameState.Start;
		activatePlayer (currentPlayer);

	}
	protected GameManager()	{	}

	public void Die()
	{
		totalEnergyBalls += currentEnergyBalls;
		if (currentScore > highScore) {
			highScore = currentScore;
		}
		this.Save ();
		this.gameState = GameState.Dead;
		UIManager.Instance.setTotalEnergyBalls (totalEnergyBalls.ToString());
		UIManager.Instance.setDmScore (currentEnergyBalls.ToString(), currentScore.ToString());

		UIManager.Instance.openDeathMenu ();
	}

	public void activatePlayer(CurrentPlayer changeTo){
		currentPlayer = changeTo;
		Save ();
		switch (currentPlayer) {
			case CurrentPlayer.player_0:
				players[0].SetActive (true);
				players[1].SetActive (false);
				break;
			case CurrentPlayer.player_1:
				players[0].SetActive (false);
				players[1].SetActive (true);
				break;
		}

		//To restore the position of the selected player back to starting
		controllerMovement.Instance.Reset ();
	}
	public void Pause(){
		pause = !pause;

		UIManager.Instance.pauseMenu.SetActive (pause);
		UIManager.Instance.gamePlayMenu.SetActive (!pause);
		UIManager.Instance.countDown.SetActive (!pause);

		if (pause) {
			Time.timeScale = 0;
			this.gameState = GameState.Pause;
			//SoundManager.instance.bgmSound.Pause ();
		} 
		else {
			StartCoroutine (getReady());
		}
			
	}
	public void Save(){
		PlayerPrefs.SetInt ("CurrentPlayer",(int)currentPlayer);
		PlayerPrefs.SetInt ("EnergyBalls",totalEnergyBalls);
		PlayerPrefs.SetInt ("playerAvailability", playerAvailability);
		PlayerPrefs.SetInt ("HighScore", highScore);
	}

	public void Reset(){
		pause = false;

		this.gameState = GameState.Start;
		TileManager.Instance.Reset ();
		CameraFollow.Instance.Reset ();
		controllerMovement.Instance.Reset ();

	}
	IEnumerator getReady(){

		UIManager.Instance.countText.text = "3";
		yield return new WaitForSecondsRealtime (1f);
		UIManager.Instance.countText.text = "2";
		yield return new WaitForSecondsRealtime (1f);
		UIManager.Instance.countText.text = "1";
		yield return new WaitForSecondsRealtime (1f);
		UIManager.Instance.countText.text = "GO";
		yield return new WaitForSecondsRealtime (0.2f);

		UIManager.Instance.countDown.SetActive (false);
		Time.timeScale = 1;
		this.gameState = GameState.Playing;
		SoundManager.instance.bgmSound.UnPause ();

	}
		
}
