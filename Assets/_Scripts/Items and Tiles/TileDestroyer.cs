﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Tile destroyer.
/// </summary>
public class TileDestroyer : MonoBehaviour {

	public GameObject TileDestroyerPoint;
	// Use this for initialization
	void Start () {
		TileDestroyerPoint = GameObject.Find ("TileDestroyerPoint");
	}
		
	// Update is called once per frame
	void Update () {

		if (transform.position.z + TileManager.TileLength < TileDestroyerPoint.transform.position.z) {
			gameObject.SetActive(false);
		}
	}
		
}
