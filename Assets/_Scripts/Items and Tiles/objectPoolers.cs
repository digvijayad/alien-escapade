﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class objectPoolers : MonoBehaviour {

	/*Prefab that needs to be object Pooled*/
	public GameObject pooledObject;

	public int pooledAmount;

	List<GameObject> pooledObjects;
	// Use this for initialization
	void Start () {

		if( pooledObject == null )
		{
			Debug.Log("ObjectPoolere needs the pooled Object Prefab!");
			enabled = false;
			return;
		}

		pooledObjects = new List<GameObject> ();

		for (int i = 0; i < pooledAmount; i++) {
			GameObject obj = Instantiate (pooledObject) as GameObject;
			//So that all tiles are created under TilesManager in hierarchy
			obj.transform.SetParent (TileManager.Instance.transform);
			obj.isStatic = true;
			obj.SetActive (false);
			pooledObjects.Add (obj);
		}
	}

	public GameObject GetPooledObject(){
		for (int i = 0; i < pooledObjects.Count; i++) {
			if (!pooledObjects [i].activeInHierarchy) {
				return pooledObjects [i];
			}
		}

		GameObject obj = Instantiate (pooledObject) as GameObject;
		obj.transform.SetParent (TileManager.Instance.transform);
		obj.isStatic = true;
		obj.SetActive (false);
		pooledObjects.Add (obj);

		return obj;
	}
	

}
