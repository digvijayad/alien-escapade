﻿using UnityEngine;
using System.Collections;

public class EnergyPrefabDestroyer : MonoBehaviour {

	private int safeDist = 10;
	private Transform destructionPoint;
	// Use this for initialization
	void Start () {
		destructionPoint = GameObject.Find ("TileDestroyerPoint").transform;
	}
	
	// Update is called once per frame
	void Update () {
		if (transform.position.z + safeDist < destructionPoint.position.z) {
			this.gameObject.SetActive (false);
		}
			
	}


}
