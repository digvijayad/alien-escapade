﻿using UnityEngine;
using System.Collections;

public class EnergyScript : MonoBehaviour {

	private Transform destructionPoint;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		transform.Rotate ( new Vector3 (0,3f,0));
	}
		
	void OnTriggerEnter(Collider other)
	{
		
		if(other.gameObject.tag.Equals("Player")){
			SoundManager.instance.PlayingSound ("energy");
			GameManager.Instance.currentEnergyBalls++;
			UIManager.Instance.setEnergyBalls (GameManager.Instance.currentEnergyBalls.ToString());
			this.gameObject.SetActive (false);
		}
	}
}
